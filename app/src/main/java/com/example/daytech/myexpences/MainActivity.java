package com.example.daytech.myexpences;

import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.example.daytech.myexpences.Adapters.RecyclerViewAdapter;
import com.example.daytech.myexpences.Dialogs.CreditDialog;
import com.example.daytech.myexpences.Dialogs.DebitDialog;
import com.example.daytech.myexpences.Fragments.ClsSuccessErrorFrag;
import com.example.daytech.myexpences.Fragments.FragAddRecordsByDate;
import com.example.daytech.myexpences.Fragments.FragOldRecordsMonthList;
import com.example.daytech.myexpences.POJO.Expences;
import com.example.daytech.myexpences.Utils.DBUtil;
import com.google.android.gms.ads.MobileAds;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView mRecyclerViewExpences;
    private RecyclerViewAdapter mRecyclerViewAdapter;
    private ArrayList<Expences> mListExpences;
    private FloatingActionButton fabMenu;
    private boolean isFABOpen = false;
    private AppCompatTextView txtBalance;
    private LinearLayout llDebit, llCredit;
    private double mBalance;
    private ViewFlipper viewFlipper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);
        MobileAds.initialize(this, getResources().getString(R.string.add_mob_app_id));

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mRecyclerViewExpences = findViewById(R.id.mRecyclerViewExpences);
        fabMenu = findViewById(R.id.fabMenu);
        llDebit = findViewById(R.id.llDebit);
        llCredit = findViewById(R.id.llCredit);
        txtBalance = findViewById(R.id.txtBalance);
        viewFlipper = findViewById(R.id.viewFlipper);

        mListExpences = new ArrayList<>();

        Log.e("ddd", "month : " + Calendar.getInstance().get(Calendar.MONTH));

        mListExpences.addAll(DBUtil.getInstance(MainActivity.this).getAllExpences());
        Collections.reverse(mListExpences);

        mRecyclerViewAdapter = new RecyclerViewAdapter(mListExpences);
        mRecyclerViewExpences.setAdapter(mRecyclerViewAdapter);
        mRecyclerViewExpences.setLayoutManager(new LinearLayoutManager(this, LinearLayout.VERTICAL, false));
        updateBalance();
        flipView();

        mRecyclerViewAdapter.setOnItemLongPress(position -> {
            LinearLayout llEdit, llDelete;

            final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(MainActivity.this);
            View sheetView = getLayoutInflater().inflate(R.layout.lay_bottom_sheet_dialog, null);
            llEdit = sheetView.findViewById(R.id.llEdit);
            llDelete = sheetView.findViewById(R.id.llDelete);
            mBottomSheetDialog.setContentView(sheetView);
            mBottomSheetDialog.show();

            llEdit.setOnClickListener(view -> {

                mBottomSheetDialog.dismiss();
                if (position == -1) {
                    notifyListChanged();
                } else {
                    Expences expences = mListExpences.get(position);

                    if (expences.getFlag() == 0) {
                        DebitDialog debitDialog = new DebitDialog(MainActivity.this, expences, mBalance);
                        debitDialog.setOnDebitSaveListenr(() -> {

                            animateSuccess("Edited Successfuly");
                            notifyListChanged();
                        });

                        debitDialog.show();
                    } else {

                        CreditDialog creditDialog = new CreditDialog(MainActivity.this, expences);
                        creditDialog.setOnDebitSaveListenr(() -> {

                            animateSuccess("Edited Successfuly");
                            notifyListChanged();
                        });
                        creditDialog.show();
                    }

                }
            });

            llDelete.setOnClickListener(view -> {
                mBottomSheetDialog.dismiss();
                final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(MainActivity.this);
                bottomSheetDialog.setContentView(R.layout.lay_bottom_sheet_delete_dialog);
                bottomSheetDialog.show();

                AppCompatTextView txtCancel, txtDelete;
                txtCancel = bottomSheetDialog.findViewById(R.id.txtCancel);
                txtDelete = bottomSheetDialog.findViewById(R.id.txtDelete);

                txtDelete.setOnClickListener(view1 -> {
                    bottomSheetDialog.dismiss();
                    Expences expences = mListExpences.get(position);
                    double bal = 0;
                    if (expences.getFlag() == 1) {
                        bal = mBalance - expences.getAmt();
                    }
                    if (bal >= 0) {
                        int id = expences.getId();
                        DBUtil.getInstance(MainActivity.this).delete(id);
                        notifyListChanged();
                        animateSuccess("Deleted Successfuly");
                    } else
                        Toast.makeText(MainActivity.this, "Do not have enough balance", Toast.LENGTH_LONG).show();


                });

                txtCancel.setOnClickListener(view12 -> {
                    bottomSheetDialog.dismiss();

                });


            });


        });

        fabMenu.setOnClickListener(view -> {
            if (!isFABOpen) {
                showFABMenu();
            } else {
                closeFABMenu();
            }
        });


    }

    private void flipView() {
        if (mListExpences.size() == 0) {
            viewFlipper.setDisplayedChild(1);
        } else {
            viewFlipper.setDisplayedChild(0);
        }
    }

    private void animateSuccess(String msg) {
        ClsSuccessErrorFrag clsSuccessErrorFrag = new ClsSuccessErrorFrag(msg);
        getSupportFragmentManager().beginTransaction().addToBackStack(null).add(R.id.mainLayoutContainer, clsSuccessErrorFrag)
                .commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_old_data:
                getSupportFragmentManager().beginTransaction().addToBackStack(null)
                        .add(R.id.mainLayoutContainer, new FragOldRecordsMonthList()).commit();
                break;
            case R.id.nav_current_data:
                clearBackFragStack();
                break;
            case R.id.nav_all_credits:
                addDataInList(1);
                break;
            case R.id.nav_all_debits:
                addDataInList(0);
                break;

            case R.id.nav_add_records_for_another_date:

                FragAddRecordsByDate fragAddRecordsByDate = new FragAddRecordsByDate();
                fragAddRecordsByDate.setOnRecordSave(this::notifyListChanged);
                getSupportFragmentManager().beginTransaction().addToBackStack(null)
                        .add(R.id.mainLayoutContainer, fragAddRecordsByDate).commit();

                break;
            default:
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void addDataInList(int flag) {
        clearBackFragStack();
        ArrayList<Expences> listExpences = new ArrayList<>();
        for (Expences expences : mListExpences) {
            if (expences.getFlag() == flag)
                listExpences.add(expences);
        }
        mListExpences.clear();
        mListExpences.addAll(listExpences);
        mRecyclerViewAdapter.notifyDataSetChanged();
        flipView();
    }

    private void clearBackFragStack() {
        int frgCount = getSupportFragmentManager().getBackStackEntryCount();
        while (frgCount >= 1) {
            getSupportFragmentManager().popBackStack();
            frgCount--;
        }
        notifyListChanged();
    }

    private void notifyListChanged() {

        mListExpences.clear();
        mListExpences.addAll(DBUtil.getInstance(MainActivity.this).getAllExpences());
        Collections.reverse(mListExpences);
        mRecyclerViewAdapter.notifyDataSetChanged();
        updateBalance();
        flipView();
    }

    private void updateBalance() {
        mBalance = 0;
        for (Expences expences : mListExpences) {
            if (expences.getFlag() == 0)
                mBalance -= expences.getAmt();
            else
                mBalance += expences.getAmt();
        }
        txtBalance.setText(+ String.format("%.2f", mBalance));

        if (mBalance == 0) {
            Toast.makeText(this, "Credit some amount to use the app", Toast.LENGTH_LONG).show();
        }
    }

    private void showFABMenu() {
        isFABOpen = true;
        fabMenu.setImageResource(R.drawable.ic_close_white_24dp);
        llCredit.animate().translationY(-getResources().getDimension(R.dimen.standard_70));
        llDebit.animate().translationY(-getResources().getDimension(R.dimen.standard_115));
        llDebit.setOnClickListener(MainActivity.this);
        llCredit.setOnClickListener(MainActivity.this);
    }

    private void closeFABMenu() {
        isFABOpen = false;
        fabMenu.setImageResource(R.drawable.ic_add_white_24dp);
        llCredit.animate().translationY(0);
        llDebit.animate().translationY(0);
        llCredit.setOnClickListener(null);
        llDebit.setOnClickListener(null);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llCredit:
                closeFABMenu();
                CreditDialog creditDialog = new CreditDialog(MainActivity.this, null);
                creditDialog.setOnDebitSaveListenr(() -> {
                    animateSuccess("Credited Successfuly");
                    notifyListChanged();
                });
                creditDialog.create();
                creditDialog.show();
                break;

            case R.id.llDebit:
                closeFABMenu();
                DebitDialog debitDialog = new DebitDialog(MainActivity.this, null, mBalance);
                debitDialog.setOnDebitSaveListenr(() -> {
                    animateSuccess("Debited Successfuly");
                    notifyListChanged();
                });
                debitDialog.create();
                debitDialog.show();
                break;
        }
    }
}
