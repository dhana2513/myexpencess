package com.example.daytech.myexpences.POJO;

import java.io.Serializable;

public class Expences implements Serializable {

    private int id;
    private String date;
    private String remark;
    private double amt;
    private int flag;

    public Expences(int id, String date, String remark, double amt, int flag) {
        this.id = id;
        this.date = date;
        this.remark = remark;
        this.amt = amt;
        this.flag = flag;
    }

    public int getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public String getRemark() {
        return remark;
    }

    public double getAmt() {
        return amt;
    }

    public int getFlag() {
        return flag;
    }
}
