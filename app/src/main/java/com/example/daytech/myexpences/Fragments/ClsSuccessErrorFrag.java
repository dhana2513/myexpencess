package com.example.daytech.myexpences.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.daytech.myexpences.R;


/**
 * Created by AJIT on 06/04/2017.
 */

public class ClsSuccessErrorFrag extends Fragment {
    private String msg;

    @SuppressLint("ValidFragment")
    public ClsSuccessErrorFrag(String msg) {
        this.msg = msg;
    }

    public ClsSuccessErrorFrag() {
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.error_success_dialog, container, false);
        Context mContext = getActivity();
        ImageView imv_check = view.findViewById(R.id.imv_check);
        AppCompatTextView txt_pin_in = view.findViewById(R.id.txt_pin_in);
        try {
            txt_pin_in.setText(msg);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                AnimatedVectorDrawable draw;
                draw = (AnimatedVectorDrawable) mContext.getDrawable(R.drawable.animated_check); // Insert your AnimatedVectorDrawable resource identifier
                imv_check.setImageDrawable(draw);
                assert draw != null;
                draw.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        new Handler().postDelayed(
                () -> getFragmentManager().popBackStack(),
                2000);
        return view;
    }


}
