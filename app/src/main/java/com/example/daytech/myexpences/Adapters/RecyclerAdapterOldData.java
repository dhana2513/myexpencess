package com.example.daytech.myexpences.Adapters;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.example.daytech.myexpences.POJO.Expences;
import com.example.daytech.myexpences.R;
import java.util.ArrayList;

public class RecyclerAdapterOldData extends RecyclerView.Adapter<RecyclerAdapterOldData.ExpenceHolder> {

    private ArrayList<Expences> mListExpences;

    public RecyclerAdapterOldData(ArrayList<Expences> mListExpences) {
        this.mListExpences = mListExpences;
    }

    @NonNull
    @Override
    public ExpenceHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lay_item_view_expences, null);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(5, 1, 5, 1);
        view.setLayoutParams(layoutParams);

        return new ExpenceHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @SuppressLint("SimpleDateFormat")
    @Override
    public void onBindViewHolder(@NonNull ExpenceHolder expenceHolder, int i) {

        try {
            Expences expences = mListExpences.get(i);
            String strDate = expences.getDate();

            if (expences.getFlag() == 0) {
                expenceHolder.txtName.setTextColor(Color.parseColor("#ff6a6a"));
                expenceHolder.txtDate.setTextColor(Color.parseColor("#ff6a6a"));
                expenceHolder.txtAmt.setTextColor(Color.parseColor("#ff6a6a"));
            } else {
                expenceHolder.txtName.setTextColor(Color.parseColor("#038712"));
                expenceHolder.txtDate.setTextColor(Color.parseColor("#038712"));
                expenceHolder.txtAmt.setTextColor(Color.parseColor("#038712"));
            }
            String date;
            if (strDate.length() > 12)
                date = strDate.substring(4, 10);
            else
                date = strDate;

            expenceHolder.txtName.setText(expences.getRemark());
            expenceHolder.txtDate.setText(date);
            expenceHolder.txtAmt.setText(String.format("%.2f", expences.getAmt()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mListExpences.size();
    }

    class ExpenceHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView txtDate, txtName, txtAmt;

        ExpenceHolder(@NonNull final View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.txtName);
            txtDate = itemView.findViewById(R.id.txtDate);
            txtAmt = itemView.findViewById(R.id.txtAmt);
        }
    }
}
