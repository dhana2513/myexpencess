package com.example.daytech.myexpences.Utils;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.daytech.myexpences.POJO.Expences;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;

public class DBUtil {
    private SQLiteDatabase mDbExpenceds;
    private static DBUtil dbUtil = null;
    private final String mTableName = "TBL_EXPENCES";
    private final String mOldTableName = "TBL_OLDEXPENCES";
    private String mCurrentMonthOfYear = "";

    @SuppressLint("DefaultLocale")
    private DBUtil(Context context) {
        mCurrentMonthOfYear = String.format("%02d", Calendar.getInstance().get(Calendar.MONTH)) + "_" + Calendar.getInstance().get(Calendar.YEAR);
        mDbExpenceds = new ExpencesDatabseHelper(context, "DBEXPENCES", null, 1).getWritableDatabase();

        SessionManager sessionManager = new SessionManager(context);

        if (!sessionManager.getMonth().equals(mCurrentMonthOfYear)) {
            boolean flag = !sessionManager.getMonth().isEmpty();
            sessionManager.setMonth(mCurrentMonthOfYear);
            if (flag)
                cleenRecords();
        }

    }

    public static DBUtil getInstance(Context context) {
        if (dbUtil == null) {
            dbUtil = new DBUtil(context);
        }
        return dbUtil;
    }


    public ArrayList<Expences> getAllExpences() {
        ArrayList<Expences> lisExpences = new ArrayList<>();
        @SuppressLint("Recycle")
        Cursor cursor = mDbExpenceds.rawQuery("SELECT * FROM " + mTableName, null);
        while (cursor.moveToNext()) {
            int id = cursor.getInt(0);
            String date = cursor.getString(1);
            String remark = cursor.getString(2);
            double amt = cursor.getDouble(3);
            int flag = cursor.getInt(4);

            Expences expences = new Expences(id, date, remark, amt, flag);
            lisExpences.add(expences);
        }
        cursor.close();
        return lisExpences;
    }

    public ArrayList<Expences> getAllOldExpences() {
        ArrayList<Expences> lisExpences = new ArrayList<>();
        @SuppressLint("Recycle")
        Cursor cursor = mDbExpenceds.rawQuery("SELECT * FROM " + mOldTableName, null);
        while (cursor.moveToNext()) {
            int id = cursor.getInt(0);
            String date = cursor.getString(1);
            String remark = cursor.getString(2);
            double amt = cursor.getDouble(3);
            int flag = cursor.getInt(4);

            Expences expences = new Expences(id, date, remark, amt, flag);
            lisExpences.add(expences);
        }
        cursor.close();
        return lisExpences;
    }

    public void insert(Expences expences) {
        ContentValues values = new ContentValues();

        values.put("REMARK", expences.getRemark());
        values.put("DATE", expences.getDate());
        values.put("AMT", expences.getAmt());
        values.put("FLAG", expences.getFlag());
        values.put("MONTH", mCurrentMonthOfYear);

        mDbExpenceds.insert(mTableName, null, values);

    }

    private void cleenRecords() {
        Cursor cursor = mDbExpenceds.rawQuery("SELECT * FROM " + mTableName, null);

        double balance = 0;

        while (cursor.moveToNext()) {
            String date = cursor.getString(1);
            String remark = cursor.getString(2);
            double amt = cursor.getDouble(3);
            int flag = cursor.getInt(4);
            String month = cursor.getString(5);

            if (flag == 0)
                balance -= amt;
            else
                balance += amt;

            ContentValues values = new ContentValues();

            values.put("REMARK", remark);
            values.put("DATE", date);
            values.put("AMT", amt);
            values.put("FLAG", flag);
            values.put("MONTH", month);

            mDbExpenceds.insert(mOldTableName, null, values);
        }
        cursor.close();
        mDbExpenceds.execSQL("DELETE FROM " + mTableName);
        ContentValues values = new ContentValues();

        values.put("REMARK", "Last Month Balance");
        String month = convertToMon(mCurrentMonthOfYear.substring(0, 2));
        values.put("DATE", "01 " + month);
        values.put("AMT", balance);
        values.put("FLAG", 1);
        values.put("MONTH", mCurrentMonthOfYear);

        mDbExpenceds.insert(mTableName, null, values);
    }


    public void delete(int id) {
        mDbExpenceds.delete(mTableName, "ID = ?", new String[]{id + ""});
    }

    public void update(Expences expences) {
        int id = expences.getId();
        ContentValues values = new ContentValues();
        values.put("ID", id);
        values.put("REMARK", expences.getRemark());
        values.put("DATE", expences.getDate());
        values.put("AMT", expences.getAmt());
        values.put("FLAG", expences.getFlag());

        mDbExpenceds.update(mTableName, values, "ID = ?", new String[]{id + ""});
    }

    public void deleteFromOld(int id) {
        mDbExpenceds.delete(mOldTableName, "ID = ?", new String[]{id + ""});
    }

    public ArrayList<String> getAllDebitRemarks() {

        ArrayList<String> listRemarks = new ArrayList<>();
        Cursor cursor = mDbExpenceds.rawQuery("SELECT * FROM " + mTableName, null);
        Cursor cursor1 = mDbExpenceds.rawQuery("SELECT * FROM " + mOldTableName, null);

        while (cursor.moveToNext()) {
            String remark = cursor.getString(2);
            if (cursor.getInt(4) == 0 && (!listRemarks.contains(remark)))
                listRemarks.add(remark);
        }
        while (cursor1.moveToNext()) {
            String remark = cursor1.getString(2);
            if (cursor1.getInt(4) == 0 && (!listRemarks.contains(remark)))
                listRemarks.add(remark);
        }

        cursor.close();
        cursor1.close();
        return listRemarks;
    }

    public ArrayList<String> getAllCreditRemarks() {
        ArrayList<String> listRemarks = new ArrayList<>();
        Cursor cursor = mDbExpenceds.rawQuery("SELECT * FROM " + mTableName, null);
        Cursor cursor1 = mDbExpenceds.rawQuery("SELECT * FROM " + mOldTableName, null);

        while (cursor.moveToNext()) {
            String remark = cursor.getString(2);
            if (cursor.getInt(4) == 1 && (!listRemarks.contains(remark)))
                listRemarks.add(remark);
        }
        while (cursor1.moveToNext()) {
            String remark = cursor1.getString(2);
            if (cursor1.getInt(4) == 1 && (!listRemarks.contains(remark)))
                listRemarks.add(remark);
        }

        cursor.close();
        cursor1.close();
        return listRemarks;
    }

    public HashSet<String> getAllMonths(){
        HashSet<String> hash_Set = new HashSet<String>();
        Cursor cursor = mDbExpenceds.rawQuery("SELECT * FROM " + mOldTableName, null);
        while (cursor.moveToNext()){
            String month = cursor.getString( 5);
            String mnt = convertToMon(month.substring(0, 2));
            mnt = mnt + month.substring(2);
            hash_Set.add( mnt );
        }
        return hash_Set;
    }
    public ArrayList<Expences> getRecordsByMonth(String month){
        ArrayList<Expences> listExpences = new ArrayList<>();
        String mm =  month.substring(0,3);
        String mnt = null;

        switch ( mm ){
            case "Jan":mnt = "01";break;
            case "Feb":mnt = "02";break;
            case "Mar":mnt = "03";break;
            case "Apr":mnt = "04";break;
            case "May":mnt = "05";break;
            case "Jun":mnt = "06";break;
            case "Jul":mnt = "07";break;
            case "Aug":mnt = "08";break;
            case "Sep":mnt = "09";break;
            case "Oct":mnt = "10";break;
            case "Nov":mnt = "11";break;
            case "Dec":mnt = "12";break;
        }

        mnt = mnt + month.substring(3);
        Cursor cursor = mDbExpenceds.rawQuery( "SELECT * FROM "+ mOldTableName+" WHERE MONTH = '"+mnt+"'",null);

        while (cursor.moveToNext()){
            int id = cursor.getInt(0);
            String date = cursor.getString(1);
            String remark = cursor.getString(2);
            double amt = cursor.getDouble(3);
            int flag = cursor.getInt(4);

            Expences expences = new Expences(id, date, remark, amt, flag);
            listExpences.add(expences);
        }
        cursor.close();

        return listExpences;
    }

    private String convertToMon(String month) {
        int date = Integer.parseInt(month);
        String mnt = null;
        switch (date) {
            case 1: mnt = "Jan";break;
            case 2: mnt = "Feb";break;
            case 3: mnt = "Mar";break;
            case 4: mnt = "Apr";break;
            case 5: mnt = "May";break;
            case 6: mnt = "Jun";break;
            case 7: mnt = "Jul";break;
            case 8: mnt = "Aug";break;
            case 9: mnt = "Sep";break;
            case 10:mnt = "Oct";break;
            case 11:mnt = "Nov";break;
            case 12:mnt = "Dec";break;
        }
        return mnt;
    }

    class ExpencesDatabseHelper extends SQLiteOpenHelper {

        ExpencesDatabseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            sqLiteDatabase.execSQL("CREATE TABLE " + mTableName + "(ID INTEGER PRIMARY KEY AUTOINCREMENT,DATE TEXT,REMARK TEXT,AMT REAL,FLAG INTEGER ,MONTH TEXT)");
            sqLiteDatabase.execSQL("CREATE TABLE " + mOldTableName + "(ID INTEGER PRIMARY KEY AUTOINCREMENT,DATE TEXT,REMARK TEXT,AMT REAL,FLAG INTEGER,MONTH TEXT)");
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        }
    }
}

