package com.example.daytech.myexpences.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import com.example.daytech.myexpences.POJO.Expences;
import com.example.daytech.myexpences.R;
import com.example.daytech.myexpences.Adapters.RecyclerAdapterOldData;

import java.util.ArrayList;

public class FragRecordsByMonth extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.lay_frag_show_old_data, null );
        setHasOptionsMenu( true );

        RecyclerView recyclerViewOldExpences = view.findViewById(R.id.mRecyclerViewOldExpences);
        Bundle bundle = getArguments();

        ArrayList<Expences> listExpences;
        listExpences = (ArrayList<Expences>) bundle.getSerializable("LIST");

        RecyclerAdapterOldData recyclerViewAdapter = new RecyclerAdapterOldData(listExpences);
        recyclerViewOldExpences.setAdapter(recyclerViewAdapter);
        recyclerViewOldExpences.setLayoutManager( new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false ));

        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.clear();
    }
}
