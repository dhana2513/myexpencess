package com.example.daytech.myexpences.Fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.RadioGroup;

import com.example.daytech.myexpences.POJO.Expences;
import com.example.daytech.myexpences.R;
import com.example.daytech.myexpences.Utils.DBUtil;

import java.util.Calendar;

public class FragAddRecordsByDate extends Fragment {
    private TextInputLayout tiLayoutRemark, tiLayoutAmt;
    private TextInputEditText tiEdtRemark, tiEdtAmt;
    private OnRecordSave onRecordSave;
    private int flag;
    private DatePicker datePicker;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.lay_add_records_by_time, null);
        AppCompatButton btnSave = view.findViewById(R.id.btnSave);
        tiEdtRemark = view.findViewById(R.id.tiEdtRemark);
        tiEdtAmt = view.findViewById(R.id.tiEdtAmt);
        tiLayoutRemark = view.findViewById(R.id.tiLayoutRemark);
        tiLayoutAmt = view.findViewById(R.id.tiLayoutAmt);
        setHasOptionsMenu( true);

        RadioGroup radioGroup = view.findViewById(R.id.radioGroup);
        datePicker = view.findViewById(R.id.datePicker);

            datePicker.setMaxDate(Calendar.getInstance().getTimeInMillis() );

        radioGroup.setOnCheckedChangeListener((radioGroup1, i) -> {
            if (i == R.id.radioCredit) {
                flag = 1;
            } else {
                flag = 0;
            }
        });

        btnSave.setOnClickListener(view1 -> {
            String dd = String.format("%02d",datePicker.getDayOfMonth());
            int mm = datePicker.getMonth();
            String date = null;

            switch ( mm ){
                case 0 : date = "Jan "+ dd;break;
                case 1 : date = "Feb "+ dd;break;
                case 2 : date = "Mar "+ dd;break;
                case 3 : date = "Apr "+ dd;break;
                case 4 : date = "May "+ dd;break;
                case 5 : date = "Jun "+ dd;break;
                case 6 : date = "Jul "+ dd;break;
                case 7 : date = "Aug "+ dd;break;
                case 8 : date = "Sep "+ dd;break;
                case 9 : date = "Oct "+ dd;break;
                case 10 : date = "Nov "+ dd;break;
                case 11 : date = "Dec "+ dd;break;
            }

            String remark = tiEdtRemark.getText().toString();
            String amt = tiEdtAmt.getText().toString();

            if (remark.isEmpty()) {
                tiLayoutRemark.setError("Title Should Not Be Empty");
            } else if (amt.isEmpty()) {
                tiLayoutAmt.setError("Amount Should Not Be Empty");
            } else {
                double amount = 0;
                try {
                    amount = Double.parseDouble(amt);
                } catch (Exception ignored) {
                }
                if (amount == 0) {
                    tiEdtAmt.setText("");
                    tiLayoutAmt.setError("Invalid Amount");
                } else {

                    Expences expences = new Expences(0, date, remark, amount, flag);
                    DBUtil.getInstance(getContext()).insert(expences);
                    onRecordSave.refreshList();
                    getFragmentManager().popBackStack();
                }
            }
        });

        return view;

    }

    public void setOnRecordSave(OnRecordSave onRecordSave) {
        this.onRecordSave = onRecordSave;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.clear();
    }

    public interface OnRecordSave {
        void refreshList();
    }
}
