package com.example.daytech.myexpences.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ViewFlipper;

import com.example.daytech.myexpences.R;
import com.example.daytech.myexpences.Utils.DBUtil;

import java.util.ArrayList;
import java.util.HashSet;

public class FragOldRecordsMonthList extends Fragment {
    private ListView lvListMonths;
    private ViewFlipper viewFlipper;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.lay_frag_old_records_month_list, null);
        lvListMonths = view.findViewById(R.id.lvListMonths);
        viewFlipper = view.findViewById(R.id.viewFlipper);


        HashSet<String> hashSetMonths = DBUtil.getInstance(getContext()).getAllMonths();
        ArrayList<String> listMonths = new ArrayList<>(hashSetMonths);

        lvListMonths.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_dropdown_item_1line, listMonths));
        lvListMonths.setOnItemClickListener((adapterView, view1, i, l) -> {

            ArrayList listExpences = new ArrayList(DBUtil.getInstance(getContext()).getRecordsByMonth(listMonths.get(i)));
            FragRecordsByMonth fragRecordsByMonth = new FragRecordsByMonth();
            Bundle bundle = new Bundle();
            bundle.putSerializable("LIST", listExpences);
            fragRecordsByMonth.setArguments(bundle);

            getFragmentManager().beginTransaction().addToBackStack(null)
                    .add(R.id.mainLayoutContainer, fragRecordsByMonth).commit();

        });

        if (listMonths.size() == 0) {
            viewFlipper.setDisplayedChild(1);
        } else {
            viewFlipper.setDisplayedChild(0);
        }

        return view;
    }


}
