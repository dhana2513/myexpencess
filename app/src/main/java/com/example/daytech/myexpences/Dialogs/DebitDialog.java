package com.example.daytech.myexpences.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.AppCompatButton;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.example.daytech.myexpences.POJO.Expences;
import com.example.daytech.myexpences.R;
import com.example.daytech.myexpences.Utils.DBUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class DebitDialog extends Dialog {
    private AppCompatButton btnSave;
    private TextInputLayout tiLayoutRemark, tiLayoutAmt;
    private TextInputEditText tiEdtAmt;
    private AppCompatAutoCompleteTextView autoComplteTextViewRemark;
    private OnDebitSave onDebitSave;
    private double mBalance;


    public DebitDialog(final Context context, final Expences expences, double balance) {
        super(context, R.style.Dialog);
        setTitle("Debit");
        mBalance = balance;
        setContentView(R.layout.lay_custom_dialog);
        init();
        if (expences != null) {
            autoComplteTextViewRemark.setText(expences.getRemark());
            tiEdtAmt.setText(expences.getAmt() + "");
            mBalance += expences.getAmt();
        }

        btnSave.setOnClickListener(view -> {

            String remark = autoComplteTextViewRemark.getText().toString().trim();
            String amt = tiEdtAmt.getText().toString().trim();

            if (remark.isEmpty()) {
                tiLayoutRemark.setError("Title Should Not Be Empty");
            } else if (amt.isEmpty()) {
                tiLayoutRemark.setError(null);
                tiLayoutAmt.setError("Amount Should Not Be Empty");
            } else {
                double amount = 0;
                try {
                    amount = Double.parseDouble(amt);
                } catch (Exception ignored) {
                }

                if (amount == 0) {
                    tiEdtAmt.setText("");
                    tiLayoutAmt.setError("Invalid Amount");
                } else if (mBalance - amount < 0) {
                    Toast.makeText(context, "Do not have enough balance", Toast.LENGTH_LONG).show();
                } else {

                    if (expences != null) {
                        Expences expences1 = new Expences(expences.getId(), expences.getDate(), remark, amount, 0);
                        DBUtil.getInstance(context).update(expences1);
                    } else {
                        Date date = Calendar.getInstance().getTime();
                        Expences expences12 = new Expences(0, date.toString(), remark, amount, 0);
                        DBUtil.getInstance(context).insert(expences12);
                    }
                    onDebitSave.refreshList();
                }
                dismiss();
            }
        });
    }

    private void init() {
        btnSave = findViewById(R.id.btnSave);
        autoComplteTextViewRemark = findViewById(R.id.autoComplteTextViewRemark);
        tiEdtAmt = findViewById(R.id.tiEdtAmt);
        tiLayoutRemark = findViewById(R.id.tiLayoutRemark);
        tiLayoutAmt = findViewById(R.id.tiLayoutAmt);
        DBUtil dbUtil = DBUtil.getInstance(getContext());

        ArrayList<String> listRemarks = new ArrayList<>(dbUtil.getAllDebitRemarks());
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_dropdown_item_1line, listRemarks);

        autoComplteTextViewRemark.setAdapter(adapter);
    }

    public void setOnDebitSaveListenr(OnDebitSave onDebitSave) {
        this.onDebitSave = onDebitSave;
    }

    public interface OnDebitSave {
        void refreshList();
    }


}
