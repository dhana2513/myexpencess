package com.example.daytech.myexpences.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

public class SessionManager {
    private final String mCurrentMonthKey = "CurrentMonthKey";

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;

    @SuppressLint("CommitPrefEdits")
    SessionManager(Context context) {
        mSharedPreferences = context.getSharedPreferences("ShPre_Month", Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
    }

    public void setMonth(String month) {
        mEditor.putString(mCurrentMonthKey, month).commit();
    }

    public String getMonth() {
        return mSharedPreferences.getString(mCurrentMonthKey, "");
    }


}
