package com.example.daytech.myexpences.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatButton;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.example.daytech.myexpences.POJO.Expences;
import com.example.daytech.myexpences.R;
import com.example.daytech.myexpences.Utils.DBUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class CreditDialog extends Dialog {
    private AppCompatButton btnSave;
    private TextInputLayout tiLayoutRemark, tiLayoutAmt;
    private TextInputEditText tiEdtAmt;
    private OnCreditSave onCreditSave;
    private DBUtil dbUtil;
    private AutoCompleteTextView autoComplteTextViewRemark;

    public CreditDialog(final Context context, final Expences expences) {
        super(context, R.style.Dialog);
        setTitle("Credit");
        setContentView(R.layout.lay_custom_dialog);
        init();

        if (expences != null) {
            tiEdtAmt.setText(expences.getAmt() + "");
            autoComplteTextViewRemark.setText(expences.getRemark());
        }

        btnSave.setOnClickListener(view -> {

            String remark = autoComplteTextViewRemark.getText().toString().trim();
            String amt = tiEdtAmt.getText().toString().trim();

            if (remark.isEmpty()) {
                tiLayoutRemark.setError("Remark Should Not Be Empty");
            } else if (amt.isEmpty()) {
                tiLayoutRemark.setError(null);
                tiLayoutAmt.setError("Amount Should Not Be Empty");
            } else {

                double amount = 0;
                try {
                    amount = Double.parseDouble(amt);
                } catch (Exception ignored) {
                }
                if (amount == 0) {
                    tiEdtAmt.setText("");
                    tiLayoutAmt.setError("Invalid Amount");
                } else {

                    if (expences != null) {
                        Expences expences1 = new Expences(expences.getId(), expences.getDate(), remark, amount, 1);
                        dbUtil.update(expences1);
                    } else {
                        Date date = Calendar.getInstance().getTime();
                        Expences expences12 = new Expences(0, date.toString(), remark, amount, 1);
                        dbUtil.insert(expences12);
                    }
                    onCreditSave.refreshList();
                    dismiss();
                }
            }
        });
    }

    private void init() {
        btnSave = findViewById(R.id.btnSave);
        autoComplteTextViewRemark = findViewById(R.id.autoComplteTextViewRemark);
        tiEdtAmt = findViewById(R.id.tiEdtAmt);
        tiLayoutRemark = findViewById(R.id.tiLayoutRemark);
        tiLayoutAmt = findViewById(R.id.tiLayoutAmt);
        dbUtil = DBUtil.getInstance(getContext());

        ArrayList<String> listRemarks = new ArrayList<>(dbUtil.getAllCreditRemarks());
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_dropdown_item_1line, listRemarks);

        autoComplteTextViewRemark.setAdapter(adapter);

    }


    public void setOnDebitSaveListenr(OnCreditSave onCreditSave) {
        this.onCreditSave = onCreditSave;
    }

    public interface OnCreditSave {
        void refreshList();
    }


}
